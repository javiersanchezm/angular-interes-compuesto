import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CalculoService } from './services/calculo/calculo.service';

@Component({
  selector: 'jrsm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  
  title = 'interesCompuesto';
  myForm!: FormGroup;
  
  stackedData: any;
  stackedOptions: any;

  depositoInicial!: number;
  interesAcumuladoTotal!: number;
  interesAcumuladoTotalAnual!: number;
  depositosAdicionales!: number;

  aniosArray!: number[];
  depositoInicialArray!: number[];
  depositosAdcionalesArray!: number[];
  interesAcumuladoArray!: number[];
  totalArray!: number[];

  constructor( private calculoService: CalculoService){
    this.myForm = new FormGroup({
      depositoInicial: new FormControl(''),
      tasaInteresAnual: new FormControl(''),
      aniosInvertir: new FormControl(''),
      aportacionesAdicionales: new FormControl('')
    });

    setTimeout( () => {
      this.getChart();
    },100);
  }

  ngOnInit(){
    this.myForm.valueChanges.subscribe(value => {
      this.calculoService.inicializar( value );

      setTimeout( () => {
        this.getChart();
        this.calculoService.getInteresTasaAnual();
        this.getResultados();
        this.getArray();
      }, 100)
    });

  }


  getResultados(){
    let totalTable = this.calculoService.getTotalesTable();

    this.depositoInicial = totalTable.depositoInicial;
    this.interesAcumuladoTotal = totalTable.interesAcumuladoTotal;
    this.interesAcumuladoTotalAnual = totalTable.interesAcumuladoTotalAnual;
    this.depositosAdicionales = totalTable.depositosAdicionales;
  }


  getArray(){
    let totalArray = this.calculoService.getArrays();

    this.aniosArray = totalArray.anios;
    this.depositoInicialArray = totalArray.depositoInicial;
    this.depositosAdcionalesArray = totalArray.depositoAdicional;
    this.interesAcumuladoArray = totalArray.interesAcumulado;
    this.totalArray = totalArray.total;
  }

  getChart(){
    this.stackedData = {
      labels: this.calculoService.getAniosAInvertir(),
      datasets: [
        {
          type: 'bar',
          label: 'Depósito inicial',
          backgroundColor: '#11A4AB',
          data: this.calculoService.getDepositoInicial()
        }, {
          type: 'bar',
          label: 'Depósitos adicionales acumulados',
          backgroundColor: '#4A76C6',
          data: this.calculoService.getDepositosAdicionales()
        }, {
          type: 'bar',
          label: 'Interés acumulado',
          backgroundColor: '#1486C8',
          data: this.calculoService.getInteresAcumulado()
        }
      ]
    };

    this.stackedOptions = {
        maintainAspectRatio: false,
        aspectRatio: 0.8,
        responsive: true,
        plugins: {
          tooltip: {
            mode: 'index',
            intersect: false,
          },
          legend: {
            labels: {
              //color: 'gray',
            },
          },
        },
        scales: {
          x: {
            stacked: true,
            ticks: {
              //color: 'gray',
            },
            grid: {
              //color: 'gray',
              drawBorder: false,
            },
          },
          y: {
            stacked: true,
            ticks: {
              color: 'gray',
            },
            grid: {
              //color: 'gray',
              drawBorder: false,
            },
          },
        },
    }
  };
}
