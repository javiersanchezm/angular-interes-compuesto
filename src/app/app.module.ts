import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ChartModule} from 'primeng/chart';
import { ResultadosComponent } from './components/resultados/resultados.component';
import { TablaResultadosComponent } from './components/tabla-resultados/tabla-resultados.component';
import {TableModule} from 'primeng/table';

@NgModule({
  declarations: [
    AppComponent,
    ResultadosComponent,
    TablaResultadosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ChartModule,
    ReactiveFormsModule,
    TableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
