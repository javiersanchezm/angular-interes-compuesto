import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'jrsm-resultados',
  templateUrl: './resultados.component.html',
  styleUrls: ['./resultados.component.scss']
})
export class ResultadosComponent implements OnInit {

  //valor: number = 15.33420;

  @Input() titulo!: string;
  @Input() icono!: string;
  @Input() color!: string;
  @Input() valor!: number;

  constructor() { }

  ngOnInit(): void {
  }

}
