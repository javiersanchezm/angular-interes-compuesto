import { Component, Input, OnInit } from '@angular/core';
import { CalculoService } from 'src/app/services/calculo/calculo.service';

@Component({
  selector: 'jrsm-tabla-resultados',
  templateUrl: './tabla-resultados.component.html',
  styleUrls: ['./tabla-resultados.component.scss']
})
export class TablaResultadosComponent implements OnInit {
  @Input() aniosArray!: number[];
  @Input() depositoInicialArray!: number[];
  @Input() depositosAdcionalesArray!: number[];
  @Input() interesAcumuladoArray!: number[];
  @Input() totalArray!: number[];

  constructor() {}

  ngOnInit(): void {
  }
}
