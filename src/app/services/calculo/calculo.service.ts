import { Injectable, OnChanges, OnInit } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculoService implements OnInit{
  // 1000inicial, 5interes, 17años, 100aportaciones

  depositoInicial!: number;
  tasaInteresAnual!: number;
  aniosInvertir!: number;
  aportacionesAdicionales!: number;

  interesAcumuladoTotal!: number;
  interesAcumuladoTotalAnual!: number;
  depositosAdicionales!: number;

  aniosArray!: number[];
  depositoInicialArray!: number[];
  depositosAdcionalesArray!: number[];
  interesAcumuladoArray!: number[];
  totalArray!: number[];

  constructor() {}

  ngOnInit(): void {}

  inicializar( value: any ){
    this.depositoInicial = value.depositoInicial;
    this.tasaInteresAnual = value.tasaInteresAnual;
    this.aniosInvertir = value.aniosInvertir;
    this.aportacionesAdicionales = value.aportacionesAdicionales;

    setTimeout( () => {
      //**// */
      this.getArrays();
    }, 100)
  }


  getDepositoInicial(){
    let newArr = [];
    for(let i=1; i<=this.aniosInvertir; i++){
      newArr.push(this.depositoInicial);
    }

    this.depositoInicialArray = newArr;
 
    return newArr;
  }

  getDepositosAdicionales(){
    let newArray = [];
    
    for(let i=1; i<=this.aniosInvertir; i++){
      newArray.push(this.aportacionesAdicionales*i);
    }

    this.depositosAdicionales = newArray[this.aniosInvertir-1]
    this.depositosAdcionalesArray = newArray;

    return newArray;
  }

  getInteresTasaAnual(){
    let arrayTotal = [];
    
    for(let i=1; i<=this.aniosInvertir; i++){
      let a0 = this.tasaInteresAnual / 100;
      let a1 = this.depositoInicial * (Math.pow((1 + a0), i));
      let a2 = (this.aportacionesAdicionales * (Math.pow((1+ a0), i) - 1)) / a0;
      let total = a1 + a2;
      arrayTotal.push(total);
    }

    this.interesAcumuladoTotalAnual = arrayTotal[this.aniosInvertir-1]
    this.totalArray = arrayTotal;

    return arrayTotal;
  }

  getInteresAcumulado(){
    let arrayTotal = [];
    
    for(let i=1; i<=this.aniosInvertir; i++){
      let a0 = this.tasaInteresAnual / 100;
      let a1 = this.depositoInicial * (Math.pow((1 + a0), i));
      let a2 = (this.aportacionesAdicionales * (Math.pow((1+ a0), i) - 1)) / a0;
      let total = (a1 + a2) - this.depositoInicial - this.aportacionesAdicionales*i;
      arrayTotal.push(total);
    }

    this.interesAcumuladoTotal = arrayTotal[this.aniosInvertir-1];
    this.interesAcumuladoArray = arrayTotal;

    return arrayTotal;
  }

  getAniosAInvertir(){
    let total = [];
    for(let i=1; i<=this.aniosInvertir; i++){
      total.push(i);
    }

    this.aniosArray = total;

    // this.aniosArray = total.map( val => {
    //   return {anio:val}
    // });
  
    return total;
  }

  getTotalesTable(){
    return {  
      depositoInicial: this.depositoInicial,
      interesAcumuladoTotal: this.interesAcumuladoTotal,
      interesAcumuladoTotalAnual: this.interesAcumuladoTotalAnual,
      depositosAdicionales: this.depositosAdicionales
    }
  }

  getArrays(){
    return{
      anios: this.aniosArray,
      depositoInicial: this.depositoInicialArray,
      depositoAdicional: this.depositosAdcionalesArray,
      interesAcumulado: this.interesAcumuladoArray,
      total: this.totalArray
    }
  }

}
